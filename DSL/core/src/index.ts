import Lexer from "../src/lexer";
import grammar from "../src/grammar";

var dsl, dsl1;
export default dsl = new Lexer().loadGrammar(grammar);
dsl.loadData(">> Say hello!\n" +
    "{\n" +
    "    console = @import('console')\n" +
    "    <>{\n" +
    "        hello #string = \"Hello World!\"\n" +
    "        ..console.log(hello)\n" +
    "    }\n" +
    "}");
dsl.processAll();
console.log('eeeeeee', dsl);
